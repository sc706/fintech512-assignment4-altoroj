package com.ibm.security.appscan.altoromutual.model;

import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.util.Hashtable;

import static org.junit.jupiter.api.Assertions.*;

public class MoneyWrapper {

    public static class Money implements Expression {
        protected double amount;
        protected String currency;
        protected String symbol;

        public Money(double amount, String currency, String symbol) {
            this.amount = amount;
            this.currency = currency;
            this.symbol = symbol;
        }

        protected double amount() {
            return amount;
        }

        protected String currency(){
            return currency;
        }

        protected String symbol(){
            return symbol;
        }

        public double getBalance(){
            return amount;
        }

        public String getFormatBalanceString(){
            String format = (amount < 1) ? symbol+"0.00" : symbol+".00";
            String balance = new DecimalFormat(format).format(amount);
            return balance;
        }

        public static Money dollar(double amount) {
            return new Dollar(amount, "USD", "$");
        }
        public static Money franc(double amount) {
            return new Franc(amount,"CHF", "F");
        }

        public boolean equals(Object object) {
            Money money = (Money) object;
            return amount == money.amount
                    && this.currency == money.currency;
        }

        @Override
        public Money reduce(Bank bank, String to) {
            int rate = (currency.equals("CHF") && to.equals("USD")) ? 2 : 1;
            return new Money(amount / rate, to, symbol);
        }

        @Override
        public String toString() {
            return "Money{" +
                    "amount=" + amount +
                    ", currency='" + currency + '\'' +
                    '}';
        }

        public Expression times(int multiplier) {
            return new Money(amount * multiplier, currency, symbol);
        }

        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }

    }

    public static class Dollar extends Money{

        public Dollar(double amount, String currency, String symbol) {
            super(amount, currency, symbol);
        }

        @Override
        protected String currency() {
            return currency;
        }

    }

    public static class Franc extends Money{

        public Franc(double amount, String currency, String symbol) {
            super(amount, currency, symbol);
        }

        @Override
        protected String currency() {
            return currency;
        }

    }

    public class Bank {

        private Hashtable rates= new Hashtable();

        Money reduce(Expression source, String to) {
            return source.reduce(this, to);
        }

        public int rate(String from, String to) {
            if (from.equals(to)) return 1;
            Integer rate= (Integer) rates.get(new Pair(from, to));
            return rate.intValue();
        }

        public void addRate(String from, String to, int rate) {
            rates.put(new Pair(from, to), new Integer(rate));
        }
    }

    public static class Sum implements Expression{
        Expression augend;
        Expression addend;

        public Expression plus(Expression addend) {
            return new Sum(this, addend);
        }

        public Sum(Expression augend, Expression addend) {
            this.augend= augend;
            this.addend= addend;
        }

        public Expression times(int multiplier) {
            return new Sum(augend.times(multiplier),addend.times(multiplier));
        }

        @Override
        public Money reduce(Bank bank, String to){
            double amount= augend.reduce(bank, to).amount
                    + addend.reduce(bank, to).amount;
            return new Money(amount, to, augend.reduce(bank, to).symbol);
        }
    }

    private class Pair {
        private String from;
        private String to;

        Pair(String from, String to) {
            this.from= from;
            this.to= to;
        }

        @Override
        public boolean equals(Object object) {
            Pair pair= (Pair) object;
            return from.equals(pair.from) && to.equals(pair.to);
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }

    @Test
    public void testMultiplication() {
        Money five = Money.dollar(5);
        assertEquals(Money.dollar(10), five.times(2));
        assertEquals(Money.dollar(15), five.times(3));
    }

    @Test
    public void testFrancMultiplication() {
        Money five = Money.franc(5);
        assertEquals(Money.franc(10), five.times(2));
        assertEquals(Money.franc(15), five.times(3));
    }

    @Test
    public void testEquality() {
        assertTrue(Money.dollar(5).equals(Money.dollar(5)));
        assertFalse(Money.dollar(5).equals(Money.dollar(6)));
        assertFalse(Money.franc(5).equals(Money.dollar(5)));
    }

    @Test
    public void testCurrency() {
        assertEquals("USD", Money.dollar(1).currency());
        assertEquals("CHF", Money.franc(1).currency());
    }

    @Test
    public void testSymbol() {
        assertEquals("$", Money.dollar(1).symbol());
        assertEquals("F", Money.franc(1).symbol());
    }

    @Test
    public void testBalance() {
        assertEquals(1, Money.dollar(1).getBalance());
        assertEquals(2, Money.franc(2).getBalance());
    }

    @Test
    public void testFormatBalance() {
        assertEquals("$0.02", Money.dollar(0.02).getFormatBalanceString());
        assertEquals("F1.32", Money.franc(1.32).getFormatBalanceString());
    }

    @Test
    public void testDifferentClassEquality() {
        assertTrue(new Money(10, "CHF", "F").equals(new Franc(10, "CHF", "F")));
    }

    @Test
    public void testSimpleAddition() {
        Money five= Money.dollar(5);
        Expression sum= five.plus(five);
        Bank bank= new Bank();
        Money reduced= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(10), reduced);
    }

    @Test
    public void testPlusReturnsSum() {
        Money five= Money.dollar(5);
        Expression result= five.plus(five);
        Sum sum= (Sum) result;
        assertEquals(five, sum.augend);
        assertEquals(five, sum.addend);
    }

    @Test
    public void testReduceSum() {
        Expression sum= new Sum(Money.dollar(3), Money.dollar(4));
        Bank bank= new Bank();
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(7), result);
    }

    @Test
    public void testReduceMoney() {
        Bank bank= new Bank();
        Money result= bank.reduce(Money.dollar(1), "USD");
        assertEquals(Money.dollar(1), result);
    }

    @Test
    public void testReduceMoneyDifferentCurrency() {
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Money result= bank.reduce(Money.franc(2), "USD");
        assertEquals(Money.dollar(1), result);
    }

    @Test
    public void testIdentityRate() {
        assertEquals(1, new Bank().rate("USD", "USD"));
        assertEquals(1, new Bank().rate("CHF", "CHF"));
    }

    @Test
    public void testMixedAddition() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Money result= bank.reduce(fiveBucks.plus(tenFrancs), "USD");
        assertEquals(Money.dollar(10), result);
    }

    @Test
    public void testSumPlusMoney() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Expression sum= new Sum(fiveBucks, tenFrancs).plus(fiveBucks);
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(15), result);
    }

    @Test
    public void testSumTimes() {
        Expression fiveBucks= Money.dollar(5);
        Expression tenFrancs= Money.franc(10);
        Bank bank= new Bank();
        bank.addRate("CHF", "USD", 2);
        Expression sum= new Sum(fiveBucks, tenFrancs).times(2);
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(20), result);
    }

}
