package com.ibm.security.appscan.altoromutual.model;

public interface Expression {
    MoneyWrapper.Money reduce(MoneyWrapper.Bank bank, String to);

    Expression plus(Expression addend);

    Expression times(int multiplier);
}
