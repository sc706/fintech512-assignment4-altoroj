package com.ibm.security.appscan.altoromutual.api;


import com.ibm.security.appscan.altoromutual.filter.ApiAuthFilter;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("api")
public class AltoroAPI extends ResourceConfig{
	public AltoroAPI(){
		packages("com.ibm.security.appscan.altoromutual.api");
		register(ApiAuthFilter.class);
	}
}
